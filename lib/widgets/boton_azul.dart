import 'package:flutter/material.dart';

class BotonAzul extends StatelessWidget {
  final Function onPressed;
  final String text;

  const BotonAzul({Key key, this.onPressed, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: RaisedButton(
      onPressed: this.onPressed,
      elevation: 2,
      highlightElevation: 5,
      // 11,108,184
      color: Color.fromRGBO(11, 108, 184, 0.5),
      shape: StadiumBorder(),
      child: Container(
        width: double.infinity,
        child: Center(
          child: Text(
            this.text,
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
        ),
      ),
    ));
  }
}
