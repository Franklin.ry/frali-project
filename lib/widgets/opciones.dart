import 'package:flutter/material.dart';

class Opciones extends StatelessWidget {
  final String ruta;

  const Opciones({Key key, this.ruta}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 250),
      child: Center(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              GestureDetector(
                  child: Text(
                    'Acceso',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, this.ruta);
                  }),
              GestureDetector(
                  child: Text(
                    'Registro',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, this.ruta);
                  }),
            ]),
      ),
    );
  }
}
