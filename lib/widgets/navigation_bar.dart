import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 660, bottom: 10.0),
      child: CurvedNavigationBar(
        key: _bottomNavigationKey,
        height: 50.0,

        items: <Widget>[
          Icon(Icons.home, size: 30),
          Icon(Icons.ac_unit, size: 30),
          Icon(Icons.ac_unit, size: 30),
          Icon(Icons.call_split, size: 30),
          Icon(Icons.perm_identity, size: 30),
        ],
        color: Colors.white,
        buttonBackgroundColor: Colors.white,
        backgroundColor: Colors.blueAccent,
        // animationCurve: Curves.easeInOut,
        animationDuration: Duration(milliseconds: 600),
        onTap: (index) {
          print('tap');
          setState(() {});
        },
        letIndexChange: (index) => true,
      ),
    );
  }
}
