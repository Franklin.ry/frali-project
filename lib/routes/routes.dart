//rutas de app
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:frali/pages/habitaciones_page.dart';
import 'package:frali/pages/loading_page.dart';
import 'package:frali/pages/login_page.dart';
import 'package:frali/pages/register_page.dart';
import 'package:frali/pages/splashscreen-page.dart';
import 'package:frali/pages/usuarios_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'usuarios': (_) => UsuariosPage(),
  'habitaciones': (_) => HabitacionesPage(),
  'login': (_) => LoginPage(),
  'register': (_) => RegisterPage(),
  'loading': (_) => LoadingPage(),
  'splashscreen': (_) => SplashScreen(),
};
