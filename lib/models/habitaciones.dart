// To parse this JSON data, do
//
//     final habitacionResponse = habitacionResponseFromJson(jsonString);

import 'dart:convert';

HabitacionResponse habitacionResponseFromJson(String str) => HabitacionResponse.fromJson(json.decode(str));

String habitacionResponseToJson(HabitacionResponse data) => json.encode(data.toJson());

class HabitacionResponse {
    HabitacionResponse({
        this.total,
        this.habitaciones,
    });

    int total;
    List<Habitacione>  habitaciones;

    factory HabitacionResponse.fromJson(Map<String, dynamic> json) => HabitacionResponse(
        total: json["total"],
        habitaciones: List<Habitacione>.from(json["habitaciones"].map((x) => Habitacione.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "habitaciones": List<dynamic>.from(habitaciones.map((x) => x.toJson())),
    };
}

class Habitacione {
    Habitacione({
        this.estado,
        this.precio,
        this.id,
        this.capacidad,
        this.piso,
        this.servicio,
        this.categoria,
        this.img,
    });

    bool estado;
    int precio;
    String id;
    int capacidad;
    int piso;
    Categoria servicio;
    Categoria categoria;
    String img;

    factory Habitacione.fromJson(Map<String, dynamic> json) => Habitacione(
        estado: json["estado"],
        precio: json["precio"],
        id: json["_id"],
        capacidad: json["capacidad"],
        piso: json["piso"],
        servicio: Categoria.fromJson(json["servicio"]),
        categoria: Categoria.fromJson(json["categoria"]),
        img: json["img"] == null ? null : json["img"],
    );

    Map<String, dynamic> toJson() => {
        "estado": estado,
        "precio": precio,
        "_id": id,
        "capacidad": capacidad,
        "piso": piso,
        "servicio": servicio.toJson(),
        "categoria": categoria.toJson(),
        "img": img == null ? null : img,
    };
}

class Categoria {
    Categoria({
        this.id,
        this.nombre,
    });

    String id;
    String nombre;

    factory Categoria.fromJson(Map<String, dynamic> json) => Categoria(
        id: json["_id"],
        nombre: json["nombre"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "nombre": nombre,
    };
}
