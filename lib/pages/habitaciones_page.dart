import 'package:flutter/material.dart';
import 'package:frali/models/habitaciones.dart';
import 'package:frali/services/habitaciones_services.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HabitacionesPage extends StatefulWidget {
  @override
  _HabitacionesPageState createState() => _HabitacionesPageState();
}

class _HabitacionesPageState extends State<HabitacionesPage> {
  final habitacionesService = new HabitacionesService();
  List<Habitacione> habitaciones = [];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/fondousuarios.jpg'),
          fit: BoxFit.cover,
        )),
        child: Container(
          margin: EdgeInsets.only(top: 250.0, bottom: 20.0),
          child: SmartRefresher(
            controller: _refreshController,
            enablePullDown: true,
            child: _listHabitacionres(),
            onRefresh: _cargarHabitaciones,
            header: WaterDropHeader(
              complete: Icon(Icons.check, color: Colors.blue[400]),
              waterDropColor: Colors.blue[400],
            ),
          ),
        ),
      ),
    );
  }

  ListView _listHabitacionres() {
    return ListView.separated(
        physics: BouncingScrollPhysics(),
        itemBuilder: (_, i) => _habitacionListitle(habitaciones[i]),
        separatorBuilder: (_, i) => Divider(),
        itemCount: habitaciones.length);
  }

  ListTile _habitacionListitle(Habitacione habitacion) {
    return ListTile(
      title: Text('Habitacion'),
      leading: CircleAvatar(
        child: Image.network(
            'https://cf.bstatic.com/images/hotel/max1024x768/141/141546423.jpg'),
        backgroundColor: Colors.blue[100],
      ),
      subtitle: Container(
          margin: EdgeInsets.only(right: 160.0),
          child: Column(children: <Widget>[
            Text('${habitacion.categoria.nombre}'),
            Row(children: <Widget>[
              Expanded(
                child: Icon(Icons.star, color: Colors.amber),
              ),
              Expanded(
                child: Icon(Icons.star, color: Colors.amber),
              ),
              Expanded(
                child: Icon(Icons.star, color: Colors.amber),
              )
            ]),
            Text('${habitacion.servicio.nombre}'),
            Text(
              (habitacion.estado) ? 'Disponible' : 'Ocupado',
              style: TextStyle(color: Colors.red),
            ),
            Text('${habitacion.precio}')
          ])),
      trailing: Container(
        margin: EdgeInsets.only(top: 30),
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          color: habitacion.estado ? Colors.green : Colors.red,
          borderRadius: BorderRadius.circular(100),
        ),
      ),
    );
  }

  @override
  void initState() {
    this._cargarHabitaciones();
    super.initState();
  }

  _cargarHabitaciones() async {
    this.habitaciones = await habitacionesService.getHabitaciones();
    setState(() {});
    _refreshController.refreshCompleted();
  }
}
