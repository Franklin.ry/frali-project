import 'dart:async';

import 'package:flutter/material.dart';
import 'package:frali/pages/info.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 3),
        () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => Info())));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(27, 46, 81, 12),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'assets/logo.png',
            height: 150.0,
          ),
          SizedBox(
            height: 100.0,
          ),
          Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
            Color.fromRGBO(27, 46, 81, 0),
            Color.fromRGBO(11, 108, 124, 0)
          ])))
        ],
      ),
    );
  }
}
