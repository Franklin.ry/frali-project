import 'package:flutter/material.dart';
import 'package:frali/helpers/mostrar_alerta.dart';
import 'package:frali/services/auth_services.dart';
import 'package:frali/widgets/boton_azul.dart';
import 'package:frali/widgets/custom_input.dart';
import 'package:frali/widgets/logo.dart';
import 'package:frali/widgets/opciones.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffFFFFFF),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/fondo.jpg'),
            fit: BoxFit.cover,
          )),
          child: SafeArea(
              child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              height: MediaQuery.of(context).size.height * 100,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[Logo(), Opciones(ruta: 'login'), _Form()],
              ),
            ),
          )),
        ));
  }
}

// Formulario
class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final nameCtrl = TextEditingController();
  final lastnameCtrl = TextEditingController();
  final origenPaisCtrl = TextEditingController();
  final direccionCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);

    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: <Widget>[
          Text('Bienvenido/a  Frali!', style: TextStyle(fontSize: 20)),
          Text('Introduzca sus datos de registro',
              style: TextStyle(fontSize: 15)),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Expanded(
                child: CustomInput(
                  icon: Icons.perm_identity,
                  placeholder: 'Nombre',
                  keyboardType: TextInputType.emailAddress,
                  textController: nameCtrl,
                ),
              ),
              Expanded(
                child: CustomInput(
                  icon: Icons.perm_identity,
                  placeholder: 'Apellido',
                  keyboardType: TextInputType.name,
                  textController: lastnameCtrl,
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                  child: CustomInput(
                icon: Icons.flag,
                placeholder: 'País',
                keyboardType: TextInputType.streetAddress,
                textController: origenPaisCtrl,
              )),
              Expanded(
                  child: CustomInput(
                icon: Icons.directions,
                placeholder: 'Dirección',
                keyboardType: TextInputType.streetAddress,
                textController: direccionCtrl,
              ))
            ],
          ),
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textController: emailCtrl,
          ),
          CustomInput(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            textController: passCtrl,
            isPassword: true,
          ),
          BotonAzul(
            text: 'Registrarse',
            onPressed: authService.autenticando
                ? null
                : () async {
                    FocusScope.of(context).unfocus();
                    print(emailCtrl.text);
                    print(nameCtrl.text);
                    print(passCtrl.text);
                    final registroMessage = await authService.register(
                        nameCtrl.text.trim(),
                        emailCtrl.text.trim(),
                        passCtrl.text.trim());

                    if (registroMessage == true) {
                      // cambiar
                      mostrarAlerta(context, 'Registro Exitoso',
                          'Ahora podrá disfrutar de nuestros servicios');
                      // Navigator.pushReplacementNamed(context, 'login');
                    } else {
                      mostrarAlerta(context, 'Registro incorrecto. ',
                          registroMessage[0]['msg']);
                    }
                  },
          )
        ],
      ),
    );
  }
}
