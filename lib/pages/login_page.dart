import 'package:flutter/material.dart';
import 'package:frali/helpers/mostrar_alerta.dart';
import 'package:frali/services/auth_services.dart';
import 'package:frali/widgets/boton_azul.dart';
import 'package:frali/widgets/custom_input.dart';
import 'package:frali/widgets/opciones.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFFFFFF),
      body: SafeArea(
          child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          width: MediaQuery.of(context).size.width * 100,
          height: MediaQuery.of(context).size.height * 0.99,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/fondo.jpg'),
            fit: BoxFit.cover,
          )),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              children: <Widget>[Opciones(ruta: 'register'), _Form()],
            ),
          ),
        ),
      )),
    );
  }
}

// Formulario
class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);

    return Container(
      margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: <Widget>[
          Text('Bienvenido/a  Frali!', style: TextStyle(fontSize: 20)),
          Text('Introduzca sus datos de acceso',
              style: TextStyle(fontSize: 15)),
          SizedBox(height: 20),
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textController: emailCtrl,
          ),
          CustomInput(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            textController: passCtrl,
            isPassword: true,
          ),
          // Labels(),
          BotonAzul(
            text: 'Iniciar Sesión',
            onPressed: authService.autenticando
                ? null
                : () async {
                    FocusScope.of(context).unfocus(); //quita el teclado
                    final loginOk = await authService.login(
                        emailCtrl.text.trim(), passCtrl.text.trim());

                    if (loginOk) {
                      //Navegar a otro pantalla
                      Navigator.pushReplacementNamed(context, 'usuarios');
                    } else {
                      //Mostrar una alerta
                      mostrarAlerta(context, 'Login Incorrecto',
                          'Correo o Contraseña Incorrecta');
                      emailCtrl.clear();
                      passCtrl.clear();
                    }
                  },
          )
        ],
      ),
    );
  }
}
