import 'package:flutter/material.dart';
import 'package:frali/services/auth_services.dart';
import 'package:frali/services/usuarios_services.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:frali/models/usuario.dart';

class UsuariosPage extends StatefulWidget {
  @override
  _UsuariosPageState createState() => _UsuariosPageState();
}

class _UsuariosPageState extends State<UsuariosPage> {
  final usuariosService = new UsuariosService();
  List<Usuario> usuarios = [];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Gestionar Usuarios',
          style: TextStyle(color: Colors.black54),
        ),
        // elevation: 1,
        // backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(Icons.exit_to_app, color: Colors.black54),
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'login');
            AuthService.deleteToken();
          },
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10),
            child: Icon(
              Icons.check_circle,
              color: Colors.blue,
            ),
          )
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/fondousuarios.jpg'),
          fit: BoxFit.cover,
        )),
        child: Container(
          margin: EdgeInsets.only(top: 100.0, bottom: 20.0),
          child: SmartRefresher(
            controller: _refreshController,
            enablePullDown: true,
            child: _listViewUsuarios(),
            onRefresh: _cargarUsuarios,
            header: WaterDropHeader(
              complete: Icon(Icons.check, color: Colors.blue[400]),
              waterDropColor: Colors.blue[400],
            ),
          ),
        ),
      ),
    );
  }

  ListView _listViewUsuarios() {
    return ListView.separated(
        physics: BouncingScrollPhysics(),
        itemBuilder: (_, i) => _usuarioListTile(usuarios[i]),
        separatorBuilder: (_, i) => Divider(),
        itemCount: usuarios.length);
  }

  ListTile _usuarioListTile(Usuario usuario) {
    return ListTile(
      title: Text(usuario.nombre),
      leading: CircleAvatar(
        child: Text(usuario.nombre.substring(0, 1)),
        backgroundColor: Colors.blue[100],
      ),
      subtitle: Container(
          margin: EdgeInsets.only(right: 180.0),
          child: Column(children: <Widget>[Text('5 Visitas'), Text('España')])),
      trailing: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          color: usuario.estado ? Colors.green : Colors.red,
          borderRadius: BorderRadius.circular(100),
        ),
      ),
    );
  }

  @override
  void initState() {
    this._cargarUsuarios();
    super.initState();
  }

  _cargarUsuarios() async {
    this.usuarios = await usuariosService.getUsuarios();
    setState(() {});
    _refreshController.refreshCompleted();
  }
}
