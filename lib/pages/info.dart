import 'package:flutter/material.dart';

class Info extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              '¡Siéntete como en Casa!\n'
              'Dir. Av. General Enríquez y García Moreno\n'
              'Guaranda-Ecuador',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20.0,
                color: Color.fromRGBO(77, 77, 77, 10),
              ),
            ),
            Image.asset('assets/info.png', height: 300.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                    disabledColor: Color.fromRGBO(77, 77, 77, 10),
                    elevation: 10.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    color: Color.fromRGBO(11, 108, 124, 10),
                    textColor: Colors.white,
                    child: Text(
                      'Iniciar Sesión',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, 'loading');
                    }),
                RaisedButton(
                    disabledColor: Color.fromRGBO(77, 77, 77, 10),
                    elevation: 10.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    color: Color.fromRGBO(11, 108, 124, 10),
                    textColor: Colors.white,
                    child: Text(
                      'Registrarse',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, 'register');
                    }),
              ],
            ),
            RaisedButton(
                disabledColor: Color.fromRGBO(77, 77, 77, 10),
                elevation: 20.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                color: Color.fromRGBO(255, 255, 255, 0),
                textColor: Colors.black,
                child: Text(
                  'Usar ahora',
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, 'habitaciones');
                }),
          ],
        ),
      ),
    );
  }
}
