import 'package:flutter/material.dart';
import 'package:frali/services/auth_services.dart';
import 'package:provider/provider.dart';
import 'package:frali/routes/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        //Instancia global
        ChangeNotifierProvider(create: (_) => AuthService())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: 'splashscreen',
        routes: appRoutes,
      ),
    );
  }
}
