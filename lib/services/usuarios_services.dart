import 'package:frali/models/usuario.dart';
import 'package:frali/pages/usuarios_response.dart';
import 'package:frali/services/auth_services.dart';

import 'package:http/http.dart' as http;
import 'package:frali/global/environment.dart';

class UsuariosService {
  Future<List<Usuario>> getUsuarios() async {
    try {
      final resp = await http.get('${Environment.apiUrl}/usuarios', headers: {
        'Content-Type': 'application/json',
        'x-token': await AuthService.getToken()
      });

      final usuarioResponse = usuarioResponseFromJson(resp.body);
      return usuarioResponse.usuarios;
    } catch (e) {
      return [];
    }
  }
}
