import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frali/models/login_response.dart';
import 'package:frali/models/usuario.dart';
import 'package:http/http.dart' as http;
import 'package:frali/global/environment.dart';

class AuthService with ChangeNotifier {
  Usuario usuario;
  bool _autenticando = false;
  final _storage = new FlutterSecureStorage();

  bool get autenticando => this._autenticando;

  set autenticando(bool valor) {
    this._autenticando = valor;
    notifyListeners(); //notitficacion para redibuje las demás widgets
  }

  // Getters del token estatico

  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token;
  }

  static Future<void> deleteToken() async {
    final _storage = new FlutterSecureStorage();
    await _storage.delete(key: 'token');
  }

  Future<bool> login(String email, String password) async {
    this.autenticando = true;
    final data = {'correo': email, 'password': password};

    final resp = await http.post('${Environment.apiUrl}/auth/login',
        body: jsonEncode(data), headers: {'Content-Type': 'application/json'});

    // print(resp.body);
    this.autenticando = false;
    if (resp.statusCode == 200) {
      final loginResponse = loginResponseFromJson(resp.body);
      this.usuario = loginResponse.usuario;

      //Guardar token STORAGE
      await this._guardarToken(loginResponse.token);

      return true;
    } else {
      return false;
    }
  }

  Future _guardarToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future logout() async {
    await _storage.delete(key: 'token');
  }

  Future<bool> isLoggedIn() async {
    final token = await this._storage.read(key: 'token');

    final resp = await http.get('${Environment.apiUrl}/auth/login/renew',
        headers: {'Content-Type': 'application/json', 'x-token': token});

    // print(resp.body);
    if (resp.statusCode == 200) {
      final loginResponse = loginResponseFromJson(resp.body);
      this.usuario = loginResponse.usuario;

      //Guardar token STORAGE
      await this._guardarToken(loginResponse.token);

      return true;
    } else {
      this.logout();
      return false;
    }
  }

  Future register(String name, String email, String password) async {
    final String rol = 'USER_ROLE';
    this.autenticando = true; //notificar listener

    final data = {
      'nombre': name,
      'correo': email,
      'password': password,
      'rol': rol
    };

    final resp = await http.post('${Environment.apiUrl}/usuarios/new',
        body: jsonEncode(data), headers: {'Content-Type': 'application/json'});

    // print(resp.body);

    this.autenticando = false; //botones

    if (resp.statusCode == 200) {
      //mapear los datos
      // final respBody = jsonDecode(resp.body);

      return true;
    } else {
      final respBody = jsonDecode(resp.body);

      return respBody['errors'];
    }
  }
}
