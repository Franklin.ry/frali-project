import 'package:frali/global/environment.dart';
import 'package:frali/models/habitaciones.dart';
import 'package:http/http.dart' as http;

class HabitacionesService {
  Future<List<Habitacione>> getHabitaciones() async {
    try {
      final resp =
          await http.get('${Environment.apiUrl}/habitaciones', headers: {
        'Content-Type': 'application/json',
      });

      final habitacionesresponse = habitacionResponseFromJson(resp.body);

      return habitacionesresponse.habitaciones;
    } catch (e) {
      return [];
    }
  }
}
