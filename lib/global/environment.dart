import 'dart:io';

class Environment {
  static String apiUrl = Platform.isAndroid
      ? 'http://192.168.100.5:8080/api'
      : 'http://localhost:8080/api';
}
